# ProductSearchFilter

Requirements
For building and running the application you need:

JDK 1.8

Running the application locally:

Execute the main method in the ProductSearchFilterApplication.java from your IDE.

Test the api: 
http://localhost:8060/swagger-ui.html

Api Docs: 
http://localhost:8060/v2/api-docs

![ScreenShot](https://bitbucket.org/jmunta/productsearchfilter/src/master/Files.png)

