package com.walmart.product.searchfilter.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotSupportedException extends Exception {
	private static final long serialVersionUID = 1L;

	public ResourceNotSupportedException(String message) {
		super(message);
	}
}