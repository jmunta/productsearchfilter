package com.walmart.product.searchfilter.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.walmart.product.searchfilter.component.helper.LoadServiceData;
import com.walmart.product.searchfilter.dto.Product;
import com.walmart.product.searchfilter.dto.SearchParams;

@Service
public class SearchDataService {
	
	@Autowired
	private LoadServiceData loadServiceData;
	
	

	public List<Product> getResultsForSearchParams(SearchParams searchParams) {
		// TODO Auto-generated method stub
		Stream<Product> productsStream = loadServiceData.productData.stream();
		if(StringUtils.isNotBlank(searchParams.getSearchString())){
			productsStream = productsStream.filter((Product u) -> {
				return StringUtils.containsIgnoreCase(StringUtils.trim(u.getProductName()), searchParams.getSearchString())
						|| StringUtils.containsAny(u.getLongDescription(), StringUtils.splitByWholeSeparator(searchParams.getSearchString(), " ")) 
						|| StringUtils.containsAny(u.getShortDescription(), StringUtils.splitByWholeSeparator(searchParams.getSearchString(), " "));
			});
		}
		if(searchParams.getMinReviewCount() !=null && searchParams.getMinReviewCount()>=0){
			productsStream = productsStream.filter((Product u) -> u.getReviewCount()!=null).filter((Product u) -> u.getReviewCount() >= searchParams.getMinReviewCount());
			
		}
		if(searchParams.getMaxReviewCount() !=null && !(searchParams.getMaxReviewCount()<0)){
			productsStream = productsStream.filter((Product u) -> u.getReviewCount()!=null).filter((Product u) -> u.getReviewCount() <= searchParams.getMaxReviewCount());
			
		}
		if(searchParams.getMinReviewRating() !=null && searchParams.getMinReviewRating()>=0){
			productsStream = productsStream.filter((Product u) -> u.getReviewRating()!=null).filter((Product u) -> u.getReviewRating() >= searchParams.getMinReviewRating());
		}
		if(searchParams.getMaxReviewRating() !=null && !(searchParams.getMaxReviewRating()<0)){
			productsStream = productsStream.filter((Product u) -> u.getReviewRating()!=null).filter((Product u) -> u.getReviewRating() <= searchParams.getMaxReviewRating());
		}
		if(searchParams.getMinPrice() !=null && searchParams.getMinPrice()>=0){
			productsStream = productsStream.filter((Product u) -> u.getReviewRating()!=null).filter((Product u) -> u.getReviewRating() >= searchParams.getMinReviewRating());
		}
		if(searchParams.getMaxPrice() !=null && searchParams.getMaxPrice()<0){
			productsStream = productsStream.filter((Product u) -> u.getDoublePrice()!=null).filter((Product u) -> Double.compare(u.getDoublePrice(),searchParams.getMaxPrice())>=0);
		}
		if(BooleanUtils.isTrue(searchParams.getInStock())){
			productsStream = productsStream.filter((Product u) ->BooleanUtils.isTrue(u.getInStock()));
		}
		return productsStream.collect(Collectors.toList());
	}
	
	/*public List<Product> getResultsForSearchParams(SearchParams searchParams) {
		// TODO Auto-generated method stub
		List<Product> searchAndFilteredProducts = null;
		Stream<Product> productsStream = loadServiceData.productData.stream();
		List<Predicate<Product>> predicateList = new ArrayList<>();
		Predicate<Product> filterCondition = ((Product u) -> true);
		if(StringUtils.isNotBlank(searchParams.getSearchString())){
			predicateList.add((Product u) -> {
				return StringUtils.containsIgnoreCase(StringUtils.trim(u.getProductName()), searchParams.getSearchString())
						|| StringUtils.containsAny(u.getLongDescription(), StringUtils.splitByWholeSeparator(searchParams.getSearchString(), " ")) 
						|| StringUtils.containsAny(u.getShortDescription(), StringUtils.splitByWholeSeparator(searchParams.getSearchString(), " "));
			});
		}
		if(searchParams.getMinReviewCount() !=null && searchParams.getMinReviewCount()>=0){
			//productsStream = productsStream.filter((Product u) -> u.getReviewCount()!=null).filter((Product u) -> u.getReviewCount() >= searchParams.getMinReviewCount());
			filterCondition = ((Product u) -> u.getReviewCount()!=null);
			filterCondition.and((Product u) -> u.getReviewCount() >= searchParams.getMinReviewCount());
			predicateList.add(filterCondition);
		}
		if(searchParams.getMaxReviewCount() !=null && !(searchParams.getMaxReviewCount()<0)){
			//productsStream = productsStream.filter((Product u) -> u.getReviewCount()!=null).filter((Product u) -> u.getReviewCount() <= searchParams.getMaxReviewCount());
			filterCondition = ((Product u) -> u.getReviewCount()!=null);
			filterCondition.and((Product u) -> u.getReviewCount() <= searchParams.getMaxReviewCount());
			predicateList.add(filterCondition);
		}
		if(searchParams.getMinReviewRating() !=null && searchParams.getMinReviewRating()>=0){
			//productsStream = productsStream.filter((Product u) -> u.getReviewRating()!=null).filter((Product u) -> u.getReviewRating() >= searchParams.getMinReviewRating());
			filterCondition.and((Product u) -> u.getReviewRating()!=null);
			filterCondition.and((Product u) -> u.getReviewRating() >= searchParams.getMinReviewRating());
			predicateList.add(filterCondition);
		}
		if(searchParams.getMaxReviewRating() !=null && !(searchParams.getMaxReviewRating()<0)){
			filterCondition = ((Product u) -> u.getReviewRating()!=null);
			filterCondition.and((Product u) -> u.getReviewRating() <= searchParams.getMaxReviewRating());
			predicateList.add(filterCondition);
		}
		if(searchParams.getMinPrice() !=null && searchParams.getMinPrice()>=0){
			//productsStream = productsStream.filter((Product u) -> u.getReviewRating()!=null).filter((Product u) -> u.getReviewRating() >= searchParams.getMinReviewRating());
			filterCondition = ((Product u) -> u.getDoublePrice()!=null);
			filterCondition.and((Product u) -> Double.compare(u.getDoublePrice(),searchParams.getMinPrice())>=0);
			predicateList.add(filterCondition);
		}
		if(searchParams.getMaxPrice() !=null && searchParams.getMaxPrice()<0){
			filterCondition = ((Product u) -> u.getDoublePrice()!=null);
			filterCondition.and((Product u) -> Double.compare(u.getDoublePrice(),searchParams.getMaxPrice())>=0);
			predicateList.add(filterCondition);
		}
		if(BooleanUtils.isTrue(searchParams.getInStock())){
			//productsStream = productsStream.filter((Product u) -> u.getInStock());
			filterCondition = ((Product u) -> u.getInStock()!=null);
			filterCondition.and((Product u) -> u.getInStock());
			predicateList.add(filterCondition);
		}
		
		searchAndFilteredProducts = productsStream.filter(filterCondition).collect(Collectors.toList());
		return searchAndFilteredProducts;
	}*/
	
	

}
