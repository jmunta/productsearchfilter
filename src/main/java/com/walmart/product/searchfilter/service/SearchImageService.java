package com.walmart.product.searchfilter.service;

import java.util.Optional;
import java.util.function.Predicate;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.walmart.product.searchfilter.component.helper.LoadServiceData;
import com.walmart.product.searchfilter.dto.Product;
import com.walmart.product.searchfilter.service.cache.FetchExternalImageService;

@Service
public class SearchImageService {
	
	@Autowired
	private LoadServiceData loadServiceData;
	
	@Autowired
	private FetchExternalImageService fetchExternalImageService;
	
	public boolean doesImageExist(String filePath) {
		Predicate<Product> fileSearchPredicate = ((Product p) -> StringUtils.equalsIgnoreCase(StringUtils.trim(p.getProductImage()),filePath));
		Optional<Product> productsExists = loadServiceData.productData.stream().filter(fileSearchPredicate).findFirst();
		return productsExists.isPresent();
	}
	
	public byte[] getImage(String filePath) {
		return fetchExternalImageService.fetchFile(filePath);
	}
	
	

}
