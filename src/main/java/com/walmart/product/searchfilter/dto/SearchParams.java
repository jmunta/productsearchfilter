package com.walmart.product.searchfilter.dto;

import java.io.Serializable;
import java.lang.reflect.Field;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(description = "All details about the Search Params. ")
public class SearchParams implements Serializable{
	
	@ApiModelProperty(notes = "Product Name/Description search string. (Optional)", allowEmptyValue = true, required = false, value ="Search Product", dataType = "String")
	@Size(min = 10, max = 200, message = "Search String must be between 2 to 100 characters")
	private String searchString;

	@ApiModelProperty(notes = "Product Min Price products to be searched for (Optional)", allowEmptyValue = true, required = false, value ="100",  dataType = "Integer")
	@JsonFormat(shape=Shape.STRING)
	@PositiveOrZero
	@Min(value = 0, message = "Price should not be less than 0")
    @Max(value = 300000, message = "Price should not be greater than 300000")
	private Integer minPrice;

	@ApiModelProperty(notes = "Product Max Price products to be searched for (Optional)", allowEmptyValue = true, required = false, value ="200",  dataType = "Integer")
	@JsonFormat(shape=Shape.STRING)
	@PositiveOrZero
	@Min(value = 0, message = "Price should not be less than 0")
    @Max(value = 300000, message = "Price should not be greater than 300000")
	private Integer maxPrice;

	@ApiModelProperty(notes = "Product Min Review products to be searched for (Optional)", allowEmptyValue = true, required = false, value ="0",  dataType = "Integer")
	@JsonFormat(shape=Shape.STRING)
	@PositiveOrZero
	@Min(value = 0, message = "Rating should not be less than 0")
    @Max(value = 10, message = "Rating should not be greater than 5")
	private Integer minReviewRating;

	@ApiModelProperty(notes = "Product Max Review products to be searched for (Optional)", allowEmptyValue = true, required = false, value ="10", dataType = "Integer")
	@JsonFormat(shape=Shape.STRING)
	@PositiveOrZero
	@Min(value = 0, message = "Rating should not be less than 0")
    @Max(value = 10, message = "Rating should not be greater than 5")
	private Integer maxReviewRating;

	@ApiModelProperty(notes = "Product Min Review Count products to be searched for (Optional)", allowEmptyValue = true, required = false, value ="0",  dataType = "Integer")
	@JsonFormat(shape=Shape.STRING)
	@PositiveOrZero
	@Min(value = 0, message = "Rating should not be less than 0")
    @Max(value = 100000, message = "Rating should not be greater than 100000")
	private Integer minReviewCount;

	@ApiModelProperty(notes = "Product Max Review Count products to be searched for (Optional)", allowEmptyValue = true, required = false,  value ="10", dataType = "Integer")
	@JsonFormat(shape=Shape.STRING)
	@PositiveOrZero
	@Min(value = 0, message = "Rating should not be less than 0")
    @Max(value = 100000, message = "Rating should not be greater than 100000")
	private Integer maxReviewCount;

	@ApiModelProperty(notes = "Show only Product in stock (Optional). Accepts true/false", allowEmptyValue = true, required = false,  dataType = "Boolean")
	@JsonFormat(shape=Shape.STRING)
	private Boolean inStock;

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public Integer getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(Integer minPrice) {
		this.minPrice = minPrice;
	}

	public Integer getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(Integer maxPrice) {
		this.maxPrice = maxPrice;
	}

	public Integer getMinReviewRating() {
		return minReviewRating;
	}

	public void setMinReviewRating(Integer minReviewRating) {
		this.minReviewRating = minReviewRating;
	}

	public Integer getMaxReviewRating() {
		return maxReviewRating;
	}

	public void setMaxReviewRating(Integer maxReviewRating) {
		this.maxReviewRating = maxReviewRating;
	}

	public Integer getMinReviewCount() {
		return minReviewCount;
	}

	public void setMinReviewCount(Integer minReviewCount) {
		this.minReviewCount = minReviewCount;
	}

	public Integer getMaxReviewCount() {
		return maxReviewCount;
	}

	public void setMaxReviewCount(Integer maxReviewCount) {
		this.maxReviewCount = maxReviewCount;
	}

	public Boolean getInStock() {
		return inStock;
	}

	public void setInStock(Boolean inStock) {
		this.inStock = inStock;
	}
	
	@JsonIgnore
	public boolean isAllFieldsNull() {
        Field fields[] = this.getClass().getDeclaredFields();
        for (Field f : fields) {
            try {
                Object value = f.get(this);
                if (value != null) {
                    return false;
                }
            }
            catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (IllegalAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


        }
        return true;

    }

}
