package com.walmart.product.searchfilter.dto;

import java.io.Serializable;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "productId", "productName", "shortDescription", "longDescription", "price", "productImage",
		"reviewRating", "reviewCount", "inStock" })
public class Product implements Serializable {

	@JsonProperty("productId")
	private String productId;
	@JsonProperty("productName")
	private String productName;
	@JsonProperty("shortDescription")
	private String shortDescription;
	@JsonProperty("longDescription")
	private String longDescription;
	@JsonProperty("price")
	private String price;
	@JsonProperty("productImage")
	private String productImage;
	@JsonProperty("reviewRating")
	private Integer reviewRating;
	@JsonProperty("reviewCount")
	private Integer reviewCount;
	@JsonProperty("inStock")
	private Boolean inStock;
	private final static long serialVersionUID = -3633192153662451341L;
	@JsonIgnore
	private Double doublePrice;

	@JsonProperty("productId")
	public String getProductId() {
		return productId;
	}

	@JsonProperty("productId")
	public void setProductId(String productId) {
		this.productId = productId;
	}

	@JsonProperty("productName")
	public String getProductName() {
		return productName;
	}

	@JsonProperty("productName")
	public void setProductName(String productName) {
		this.productName = productName;
	}

	@JsonProperty("shortDescription")
	public String getShortDescription() {
		return shortDescription;
	}

	@JsonProperty("shortDescription")
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	@JsonProperty("longDescription")
	public String getLongDescription() {
		return longDescription;
	}

	@JsonProperty("longDescription")
	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	@JsonProperty("price")
	public String getPrice() {
		return price;
	}

	@JsonProperty("price")
	public void setPrice(String price) {
		NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
		try {
			Number number = format.parse(price);
			this.doublePrice = number.doubleValue();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.price = price;
	}

	@JsonProperty("productImage")
	public String getProductImage() {
		return productImage;
	}

	@JsonProperty("productImage")
	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

	@JsonProperty("reviewRating")
	public Integer getReviewRating() {
		return reviewRating;
	}

	@JsonProperty("reviewRating")
	public void setReviewRating(Integer reviewRating) {
		this.reviewRating = reviewRating;
	}

	@JsonProperty("reviewCount")
	public Integer getReviewCount() {
		return reviewCount;
	}

	@JsonProperty("reviewCount")
	public void setReviewCount(Integer reviewCount) {
		this.reviewCount = reviewCount;
	}

	@JsonProperty("inStock")
	public Boolean getInStock() {
		return inStock;
	}

	@JsonProperty("inStock")
	public void setInStock(Boolean inStock) {
		this.inStock = inStock;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("productId", productId).append("productName", productName)
				.append("shortDescription", shortDescription).append("longDescription", longDescription)
				.append("price", price).append("productImage", productImage).append("reviewRating", reviewRating)
				.append("reviewCount", reviewCount).append("inStock", inStock).toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(price).append(reviewRating).append(shortDescription).append(reviewCount)
				.append(productImage).append(longDescription).append(inStock).append(productName).append(productId)
				.toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Product) == false) {
			return false;
		}
		Product rhs = ((Product) other);
		return new EqualsBuilder().append(productId, rhs.productId).isEquals();
	}

	public Double getDoublePrice() {
		return doublePrice;
	}

	public void setDoublePrice(Double doublePrice) {
		this.doublePrice = doublePrice;
	}

}