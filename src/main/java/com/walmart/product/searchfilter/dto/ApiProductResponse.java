package com.walmart.product.searchfilter.dto;

import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "products", "totalProducts", "pageNumber", "pageSize", "statusCode" })
public class ApiProductResponse implements Serializable {

	@JsonProperty("products")
	private List<Product> products = null;
	@JsonProperty("totalProducts")
	private Integer totalProducts;
	@JsonProperty("pageNumber")
	private Integer pageNumber;
	@JsonProperty("pageSize")
	private Integer pageSize;
	@JsonProperty("statusCode")
	private Integer statusCode;
	private final static long serialVersionUID = 7493729967426217595L;

	@JsonProperty("products")
	public List<Product> getProducts() {
		return products;
	}

	@JsonProperty("products")
	public void setProducts(List<Product> products) {
		this.products = products;
	}

	@JsonProperty("totalProducts")
	public Integer getTotalProducts() {
		return totalProducts;
	}

	@JsonProperty("totalProducts")
	public void setTotalProducts(Integer totalProducts) {
		this.totalProducts = totalProducts;
	}

	@JsonProperty("pageNumber")
	public Integer getPageNumber() {
		return pageNumber;
	}

	@JsonProperty("pageNumber")
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	@JsonProperty("pageSize")
	public Integer getPageSize() {
		return pageSize;
	}

	@JsonProperty("pageSize")
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	@JsonProperty("statusCode")
	public Integer getStatusCode() {
		return statusCode;
	}

	@JsonProperty("statusCode")
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("products", products).append("totalProducts", totalProducts)
				.append("pageNumber", pageNumber).append("pageSize", pageSize).append("statusCode", statusCode)
				.toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(statusCode).append(pageSize).append(pageNumber).append(totalProducts)
				.append(products).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof ApiProductResponse) == false) {
			return false;
		}
		ApiProductResponse rhs = ((ApiProductResponse) other);
		return new EqualsBuilder().append(statusCode, rhs.statusCode).append(pageSize, rhs.pageSize)
				.append(pageNumber, rhs.pageNumber).append(totalProducts, rhs.totalProducts)
				.append(products, rhs.products).isEquals();
	}

}
