package com.walmart.product.searchfilter;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import com.walmart.product.searchfilter.component.helper.LoadServiceData;

@SpringBootApplication(scanBasePackages = { "com.walmart.product" })
@EnableCaching
public class ProductSearchFilterApplication {

	@Autowired
	private LoadServiceData loadServiceData;

	private static Logger logger = LogManager.getLogger(ProductSearchFilterApplication.class);
	
	public static void main(String[] args) {
		// Main application starts the tomcat server and deploys the application and load the data into memory.
		SpringApplication.run(ProductSearchFilterApplication.class, args);
	}

	@PostConstruct
	public void cacheAllProductData() {
		// Load default Data by Calling the Walmart API once for all the pages and store the data......
		try {
			logger.info(" Loading the Predefined Data");
			loadServiceData.loadData();
		} catch (Exception e) {
			logger.error("Error while loading the default walmart data with error", e);
		}
	}
}
