package com.walmart.product.searchfilter.rest.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.walmart.product.searchfilter.common.exception.ResourceNotSupportedException;
import com.walmart.product.searchfilter.dto.Product;
import com.walmart.product.searchfilter.dto.SearchParams;
import com.walmart.product.searchfilter.service.SearchDataService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/v1")
@Api(value = "Product Search/Filter System", description = "Gives Data as per the search Criteria")
public class ProductSearchController {

	@Autowired
	private SearchDataService searchDataService;
	
	/*
	 * @ApiOperation(value = "View a list of available products by page", response = List.class)
	 
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@PostMapping("/searchProducts/{pageNumber}")
    public ResponseEntity <List<Product>> getPagedSearchProducts(
        @ApiParam(value = "Page Number of Products to be fetched", required = true) @PathVariable(value = "pageNumber") Long pageNumber,
        @ApiParam(value = "Search Parameters object", required = true) @Valid @RequestBody SearchParams SearchParams) throws ResourceNotSupportedException {
       List<Product> searchList = new ArrayList<>();
        return ResponseEntity.ok(searchList);
    }
    */
	
	
	@ApiOperation(value = "View a list of available products", response = List.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@PostMapping("/searchProducts")
    public ResponseEntity <List<Product>> getAllSearchProducts(
        @ApiParam(value = "Search Parameters object", required = true) @Valid @RequestBody SearchParams searchParams) throws ResourceNotSupportedException {
       List<Product> searchList = new ArrayList<>();
       if(searchParams.isAllFieldsNull()) {
    	   throw new ResourceNotSupportedException("At least one search filed is mandatory");
       }
       if(searchParams.getMinReviewCount()!=null && searchParams.getMaxReviewCount()!=null && searchParams.getMaxReviewCount()> searchParams.getMinReviewCount()) {
    	   throw new ResourceNotSupportedException("Min Review Count greater than Max Review count. Please correct input");
       }
       if(searchParams.getMinReviewRating()!=null && searchParams.getMaxReviewRating()!=null && searchParams.getMaxReviewRating()> searchParams.getMinReviewRating()) {
    	   throw new ResourceNotSupportedException("Min Review Rating greater than Max Review Rating. Please correct input");
       }
       if(searchParams.getMinPrice()!=null && searchParams.getMaxPrice()!=null && searchParams.getMaxPrice()> searchParams.getMinPrice()) {
    	   throw new ResourceNotSupportedException("Min Price greater than Max price. Please correct input");
       }
       searchList = searchDataService.getResultsForSearchParams(searchParams);
        return ResponseEntity.ok(searchList);
    }

	
}