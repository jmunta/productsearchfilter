package com.walmart.product.searchfilter.rest.controller;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.walmart.product.searchfilter.common.exception.ResourceNotSupportedException;
import com.walmart.product.searchfilter.service.SearchImageService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/v1")
@Api(value = "Image Fetch Service", description = "Gives image data for the searched files")
public class FetchImageController {

	@Autowired
	private SearchImageService searchImageService;
	@ApiOperation(value = "Get Image For image path", response = byte[].class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@GetMapping("/fetchImage")
    public void getPagedSearchProducts(
    		HttpServletResponse response, @ApiParam(value = "Image path Page Number of Products to be fetched", required = true) @RequestParam(value = "imageName") String imageName) throws ResourceNotSupportedException, IOException {
		if(!searchImageService.doesImageExist(imageName)) {
			throw new ResourceNotSupportedException("Not a valid image path");
		}
		
		response.setContentType("image/jpeg");
        response.setHeader("Content-Disposition", "attachment; filename="+ StringUtils.substringAfterLast(imageName, "/"));
        OutputStream out = response.getOutputStream();
        out.write(searchImageService.getImage(imageName));
        out.close();
    }
    
	
	
	

	
}