package com.walmart.product.searchfilter.component.helper;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import com.walmart.product.searchfilter.dto.ApiProductResponse;
import com.walmart.product.searchfilter.dto.Product;

@Component
public class LoadServiceData {

	@Value("${dataserver.url}")
	private String prop;
	@Value("${dataserver.context.products}")
	private String productsContext;

	public final Set<Product>productData = new HashSet<>();
	
	private static Logger logger = LogManager.getLogger(LoadServiceData.class);
	
	public LoadServiceData() {

	}

	public void loadData() {
		// Load all Items
		Integer pageNumber = 0;
		Integer noOfItemsPerPage = 20;
		Integer totalNoOfPages = 1;
		String url = prop + "/" + productsContext;
		String requestUrl = null;
		RestTemplate restTemplate = new RestTemplate();
		logger.info("Url Being Triggered for data retireval : " + url);
		while(pageNumber<totalNoOfPages) {
			pageNumber++;
			requestUrl = url + "/" +pageNumber+ "/"+noOfItemsPerPage ;
			logger.info("Data Call {} Being Triggered for data retireval : {}" ,pageNumber, requestUrl);
			ApiProductResponse response = restTemplate.getForObject(requestUrl, ApiProductResponse.class);
			if(response!=null ) {
				if(pageNumber ==1) {
					totalNoOfPages = (int) Math.ceil((double)response.getTotalProducts()/noOfItemsPerPage);
				}
				if(!CollectionUtils.isEmpty(response.getProducts())) {
					productData.addAll(response.getProducts());
				}
			}
		}
		
	}
}
